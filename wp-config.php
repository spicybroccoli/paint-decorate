<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
// Include local configuration
if (file_exists(dirname(__FILE__) . '/local-config.php')) {
	include(dirname(__FILE__) . '/local-config.php');
}

// Global DB config
if (!defined('DB_NAME')) {
	define('DB_NAME', 'db142268_paint_decorate');
}
if (!defined('DB_USER')) {
	define('DB_USER', 'db142268');
}
if (!defined('DB_PASSWORD')) {
	define('DB_PASSWORD', 'fingered!');
}
if (!defined('DB_HOST')) {
	define('DB_HOST', 'internal-db.s142268.gridserver.com');
}

/** Database Charset to use in creating database tables. */
if (!defined('DB_CHARSET')) {
	define('DB_CHARSET', 'utf8');
}

/** The Database Collate type. Don't change this if in doubt. */
if (!defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '>ur0|w{!wWVVK1j7~z)!YXkg#mIzL-yZ<t+0y%}S%0<-{`S8v#37W*f)2@!s&Z|1');
define('SECURE_AUTH_KEY',  'f_m-`/rFAM;%W;i@eg&pSKAnmLu:xjac-+gS8tO+6[eA9DUmTak4yWBXUP$XL-}(');
define('LOGGED_IN_KEY',    'xEpRC&-YN^hZ)rY0v8N@.M.Zh!= .-86+*khN|<@#c|Y4*cr|ru-_zlO2|X/CM:C');
define('NONCE_KEY',        'TLu&Frq^]uyKmBQ<Z?[ujr58KRQBd$`xg$T(a%|>PkU=uRr^qc7{sWIjJn.X-+t8');
define('AUTH_SALT',        'aR[ti07}C_l;/`CK{hzxTn$1-o(8O+,?:o)0-oemD hD|1aNg9J?.}sg~T2t`X[{');
define('SECURE_AUTH_SALT', 'N,05QoWu]He=eCE4#|:#4O{4D~3T0om:RRV>lUn0=>ivP$L1[zvO,%#f|b#|(M4~');
define('LOGGED_IN_SALT',   'zuM^X01(J8I2*#F]]4^(5L-?lY]?efMx>^g]]gYLcRy=@oky%bNN[4dhR/}~Fq4k');
define('NONCE_SALT',       '*W;Q>goj2:>[8!1 .@`{T_=0..=.#i+8/#5OZk`<rOeiJS[24g!8* -|]57}:`%q');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sbm_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');



/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if (!defined('WP_DEBUG')) {
	define('WP_DEBUG', false);
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
