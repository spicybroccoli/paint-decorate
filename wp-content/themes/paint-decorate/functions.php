<?php

    function vp_scripts() {
        global $scrn;
        wp_enqueue_script( 'jquery-sticky', get_template_directory_uri() . '/js/jquery.sticky.js', array(), '1.0');
        wp_enqueue_script( 'smooth-scroll-2', get_stylesheet_directory_uri() . '/smooth-scroll.js', array(), '1.0');
        wp_enqueue_script( 'contact-form', get_template_directory_uri() . '/js/contact-form.js', array(), '1.0');
        wp_enqueue_script( 'jquery-easing', get_template_directory_uri() . '/js/jquery.easing.1.3.js', array(), '1.0');
        wp_enqueue_script( 'prettyPhoto', get_template_directory_uri() . '/js/jquery.prettyPhoto.js', array(), '1.0');
        wp_enqueue_script( 'inview', get_template_directory_uri() . '/js/jquery.inview.js', array(), '1.0');
        wp_enqueue_script( 'custom', get_template_directory_uri() . '/js/custom.js', array(), '1.0');
        wp_enqueue_script( 'isotope', get_template_directory_uri() . '/js/jquery.isotope.min.js', array(), '1.0');
        wp_enqueue_script( 'mobilemenu', get_template_directory_uri() . '/js/jquery.mobilemenu.js', array(), '1.0');
        if ( is_singular() && get_option( 'thread_comments' ) )
            wp_enqueue_script( 'comment-reply' );
    }

